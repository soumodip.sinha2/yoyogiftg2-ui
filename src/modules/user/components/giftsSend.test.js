import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftsSend from './GiftsSend';

configure({adapter: new Adapter()});

describe('<GiftsSend/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<GiftsSend />);
    });

    it('GiftsSend executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});