import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Profile from './Profile';

configure({adapter: new Adapter()});

describe('<Profile/>', () => {
    let wrapper;

    beforeEach(() => {
        const detailsObject = {
            email: "piyushnandan0@gmail.com",
            firstName: "piyush",
            id: "102216163885514183901",
            lastName: "nandan",
            picture: "https://lh4.googleusercontent.com/-Ni40dN_JRWw/AAAAAAAAAAI/AAAAAAAAAEA/XEm6mx6xDLU/photo.jpg"
        }
        wrapper = shallow(<Profile detailsObject={detailsObject}/>);
    });

    it('Profile executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});