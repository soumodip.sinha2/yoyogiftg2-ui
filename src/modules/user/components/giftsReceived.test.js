import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftsReceived from './GiftsReceived';

configure({adapter: new Adapter()});

describe('<GiftsReceived/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<GiftsReceived />);
    });

    it('GiftsReceived executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});