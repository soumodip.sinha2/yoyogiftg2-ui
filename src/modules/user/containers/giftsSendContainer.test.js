import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftsSendContainer from './GiftsSendContainer';
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "../../../store";
import { Provider } from "react-redux";

configure({adapter: new Adapter()});

describe('<GiftsSendContainer/>', () => {
    let wrapper;

    beforeEach(() => {
        const store = createStore(reducers, applyMiddleware(thunk));
        wrapper = shallow(<Provider  store={store} ><GiftsSendContainer /></Provider>);
    });

    it('GiftsSendContainer executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});