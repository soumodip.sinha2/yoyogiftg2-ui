import React, { Component } from 'react';
import Styles from '../../assets/css/Landing.module.css';
import Button from '@material-ui/core/Button';
import YoYoGiftImg from '../../assets/images/YoyoGift.png';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import { renderToStaticMarkup } from "react-dom/server";
import { withLocalize } from "react-localize-redux";
import globalTranslations from "../localization/global.json";
import landingTranslations from '../localization/landing.json';
import { Translate } from "react-localize-redux";

class Landing extends Component {

  constructor(props) {
    super(props);

    this.props.initialize({
      languages: [
        { name: "English", code: "en" },
        { name: "French", code: "fr" }
      ],
      translation: globalTranslations,
      options: { renderToStaticMarkup }
    });

    this.props.addTranslation(landingTranslations);
  }

  onLanguageChange = (e) => {
    this.props.setActiveLanguage(e.target.value)
  }

  render() {
    const { languages, setActiveLanguage} = this.props;
    return (
      <div>
        <div id="promo-ribbon" className={Styles.offerMessage}>
          <Translate id="landing.promoCode" />
        </div>
        <div className={Styles.mainSlide}>

          <label style={{ color: '#fff' }}>SELECT LANGUAGE</label> {languages.map(lang => (
            <span key={lang.code}>
              <Button variant="contained" color="default" className={Styles.languageButton} onClick={() => setActiveLanguage(lang.code)}>
                {lang.name}
              </Button>
            </span>
          ))}
          <Grid container spacing={0}>
            <Grid item xs={12} sm={6}>
              <h1 className={Styles.sectionTitle}>
                <Translate id="landing.bannerTitle" />
              </h1>
              <div className={Styles.sectionText}>
                <p><Translate id="landing.bannerSubtitle1" /></p>
                <p><Translate id="landing.bannerSubtitle2" /></p>
              </div>
              <Link to="/giftCards" onClick={this.handleClick} className={Styles.cardsBtn}>
                <Button variant="contained">
                  <Translate id="landing.exploreBtn" />
                </Button>
              </Link>
            </Grid>
            <Grid item xs={12} sm={6}>
              <img src={YoYoGiftImg} className={Styles.GiftImg} alt="YoYoImg" />
            </Grid>
          </Grid>
        </div>
        <div className={Styles.vendors}>
          <h2 className={Styles.vendorTitle}><Translate id="landing.vendorsCaption" /></h2>
          <div style={{ textAlign: 'center' }}>
            <div className={Styles.vendorContainer}>
              <img className={Styles.vendorImage} src="https://images.gyft.com/merchants/i-1466456891460_667_hd.png" alt="amazon" />
              <p className={Styles.vendorName}>Whole Foods Market</p>
            </div>
            <div className={Styles.vendorContainer}>
              <img className={Styles.vendorImage} src="https://images.gyft.com/merchants/i-188-1346844971201-60_hd.png" alt="Ebay" />
              <p className={Styles.vendorName}>Ebay</p>
            </div>
            <div className={Styles.vendorContainer}>
              <img className={Styles.vendorImage} src="https://pngimg.com/uploads/amazon/amazon_PNG21.png" alt="amazon" />
              <p className={Styles.vendorName}>Amazon</p>
            </div>
            <div className={Styles.vendorContainer}>
              <img className={Styles.vendorImage} src="http://www.adageindia.in/photo/47460433/Is-Flipkarts-New-Logo-Strong-Enough-To-Behold-Its-Brand-Identity.jpg?19122" alt="Flipkart" />
              <p className={Styles.vendorName}>Flipkart</p>
            </div>
            <div className={Styles.vendorContainer}>
              <img className={Styles.vendorImage} src="https://cms.qz.com/wp-content/uploads/2015/05/zomatos-4th-logo-2012-14.png?w=900&h=900&crop=1&strip=all&quality=75" alt="Zomato" />
              <p className={Styles.vendorName}>Zomato</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withLocalize(Landing);
