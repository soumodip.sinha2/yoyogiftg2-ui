import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import addUpdateForm from './addUpdateForm';

configure({adapter: new Adapter()});

describe('<addUpdateForm/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<addUpdateForm />);
    });

    it('addUpdateForm executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});