import React from 'react';
import { configure, shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import Adapter from 'enzyme-adapter-react-16';
import GiftShowContainer from './GiftShowContainer';
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "../../../store";
import { Provider } from "react-redux";

configure({adapter: new Adapter()});

describe('<GiftShowContainer/>', () => {
    let wrapper;

    beforeEach(() => {
        const store = createStore(reducers, applyMiddleware(thunk));
        const match = { params: { id: 1 } }
        wrapper = shallow(<Provider  store={store} ><GiftShowContainer match={match} /></Provider>);
    });

    it('GiftShowContainer executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });

    it("should render", () => {
        const store = createStore(reducers, applyMiddleware(thunk));
        const match = { params: { id: 1 } }
        const  div  =  document.createElement("app");
        ReactDOM.render(<Provider  store={store} ><GiftShowContainer match={match} /></Provider>,  div);
        ReactDOM.unmountComponentAtNode(div);
    })
});