import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftsListContainer from './GiftsListContainer';
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "../../../store";
import { Provider } from "react-redux";

configure({adapter: new Adapter()});

describe('<GiftsListContainer/>', () => {
    let wrapper;

    beforeEach(() => {
        const store = createStore(reducers, applyMiddleware(thunk));
        wrapper = shallow(<Provider  store={store} ><GiftsListContainer/></Provider>);
    });

    it('GiftsListContainer executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});