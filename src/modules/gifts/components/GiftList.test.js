import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftsList from './GiftsList';

configure({adapter: new Adapter()});

describe('<GiftsList/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<GiftsList />);
    });

    it('GiftsList executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});