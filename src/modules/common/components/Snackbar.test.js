import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MySnackBar from './SnackBar';

configure({adapter: new Adapter()});

describe('<MySnackBar/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<MySnackBar />);
    });

    it('MySnackBar executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});