import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ErrorPage from './ErrorPage';

configure({adapter: new Adapter()});

describe('<ErrorPage/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<ErrorPage />);
    });

    it('ErrorPage executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});