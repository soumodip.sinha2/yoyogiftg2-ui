import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import DatePickers from './datePicker';

configure({adapter: new Adapter()});

describe('<DatePickers/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<DatePickers />);
    });

    it('DatePickers executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});