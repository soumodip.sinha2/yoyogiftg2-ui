import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftCard from './GiftCard';

configure({adapter: new Adapter()});

describe('<GiftCard/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<GiftCard />);
    });

    it('GiftCard executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});