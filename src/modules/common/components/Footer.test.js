import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Footer from './Footer';

configure({adapter: new Adapter()});

describe('<Footer/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Footer />);
    });

    it('Footer executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});