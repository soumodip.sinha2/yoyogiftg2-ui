import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import DraggableDialog from './DraggableDialog';

configure({adapter: new Adapter()});

describe('<DraggableDialog/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<DraggableDialog />);
    });

    it('DraggableDialog executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});