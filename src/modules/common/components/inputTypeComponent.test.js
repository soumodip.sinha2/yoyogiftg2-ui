import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import InputTypeComponent from './InputTypeComponent';

configure({adapter: new Adapter()});

describe('<InputTypeComponent/>', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<InputTypeComponent />);
    });

    it('InputTypeComponent executed successfully', () => {
        expect(wrapper).toBeTruthy();
    });
});