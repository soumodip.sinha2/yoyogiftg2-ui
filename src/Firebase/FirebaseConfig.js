import * as firebase from "firebase";

// const config = {
//   apiKey: "AIzaSyDXghnPblvqkMQCdLfiyxtmNBbDv-oasFI",
//   authDomain: "librarytree-7c802.firebaseapp.com",
//   databaseURL: "https://librarytree-7c802.firebaseio.com",
//   projectId: "librarytree-7c802",
//   storageBucket: "librarytree-7c802.appspot.com",
//   messagingSenderId: "895682963147"
// };
const config = {
    apiKey: "AIzaSyAk3u_C4JPyiE76bwsdD20Gfbi_S7AOK5Y",
    authDomain: "yoyogift-55ae9.firebaseapp.com",
    databaseURL: "https://yoyogift-55ae9.firebaseio.com",
    projectId: "yoyogift-55ae9",
    storageBucket: "yoyogift-55ae9.appspot.com",
    messagingSenderId: "606991360122",
    appId: "1:606991360122:web:80c732460904b37e"
  };
firebase.initializeApp(config);

const googleOAuth = new firebase.auth.GoogleAuthProvider();

const auth = firebase.auth();

export default {
    googleOAuth,
    auth
}